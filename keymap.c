/* Copyright 2015-2017 Jack Humbert
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include QMK_KEYBOARD_H
#include "muse.h"

#define ________ KC_NO
#define SCLN_PUNCT LT(_PUNCTUATION, KC_SCLN)

#define IDE_J LT(_IDE, KC_J)              // Activate IDE Layer by holding 'J'
#define STEP_IN LSFT(KC_F8)               // Debugger: Step in
#define STEP_OUT KC_F7                    // Debugger: Step out
#define STEP_OVR KC_F8                    // Debugger: Step over
#define EVALUATE LALT(LSFT(8))            // Debugger: evaluate expression
#define CONTINUE KC_F9                    // Continue debugger
#define MUTE_BRP LALT(KC_F9)              // Mute all break points
#define CLOSE_WIN LALT(KC_F4)             // Close window

enum planck_layers {
  _QWERTY,
  _FUNC,
  _NUM,
  _PUNCTUATION,
  _F,
  _IDE,
  _NAV
};

enum custom_keycodes {
  URL_CATALOGORDER = SAFE_RANGE,
  URL_CHECKOUTTUNNEL,
  PASSWORD,
  ESC_TAB,
  ALT_SFT_TAB
};
	
enum unicode_names {
  oe,
  OE,
  ae,
  AE,
  ue,
  UE,
  ss
};

const uint32_t PROGMEM unicode_map[] = {
    [oe]  = 0x00F6, // ö
    [OE]  = 0x00D6, // Ö
    [ae]  = 0x00E4, // ä
    [AE]  = 0x00C4, // Ä
    [ue]  = 0x00FC, // ü
    [UE]  = 0x00DC, // Ü
    [ss]  = 0x00DF, // ß
};

bool is_nav_layer_active = false;

const uint16_t PROGMEM keymaps[][MATRIX_ROWS][MATRIX_COLS] = {

[_QWERTY] = LAYOUT_planck_grid(
    ESC_TAB,    KC_Q,    KC_W,    KC_E,           KC_R,     KC_T,     KC_Y,   KC_U,    KC_I,     KC_O,              KC_P,               KC_DEL,
    MO(_FUNC),  KC_A,    KC_S,    KC_D, LT(_NAV, KC_F),     KC_G,     KC_H,  IDE_J,    KC_K,     KC_L,        SCLN_PUNCT,              KC_QUOT,
    KC_LSFT,    KC_Z,    KC_X,    KC_C,           KC_V,     KC_B,     KC_N,   KC_M, KC_COMM,   KC_DOT,           KC_SLSH, MT(MOD_LSFT, KC_ENT),
    KC_LCTL, KC_LGUI,  KC_TAB, KC_LALT,        KC_BSPC, TT(_NUM),   MO(_F), KC_SPC, KC_LEAD,    KC_NO, LT(_NUM, KC_DOWN),      LT(_NUM, KC_UP)
),
[_FUNC] = LAYOUT_planck_grid(
      KC_TAB, ________,  KC_PGUP, ________, ________, ________, ________, ________, ________,    X(ss), XP(ue,UE),    KC_INS,
    ________, ________,  KC_PGDN,  KC_LSFT,  KC_LCTL, ________,  KC_LEFT,  KC_DOWN,    KC_UP,  KC_RGHT, XP(oe,OE), XP(ae,AE),
    ________, ________, ________, ________, ________, ________, ________, ________, ________, ________,   KC_MINS,   KC_VOLU,
       RESET, ________, ________, ________,  KC_HOME, ________,   KC_DEL,   KC_END, ________, ________,  ________,   KC_VOLD
),
[_NUM] = LAYOUT_planck_grid(
    ________,          ________,            ________, ________, ________,    ________, ________, KC_7   ,  KC_8   ,     KC_9, ________, ________,
    ________,  URL_CATALOGORDER,  URL_CHECKOUTTUNNEL, ________, ________,    ________, ________, KC_4   ,  KC_5   ,     KC_6, ________, ________,
    ________,          PASSWORD,            ________, ________, ________,    ________, ________, KC_1   ,  KC_2   ,     KC_3, ________, ________,
    ________,          ________,            ________, ________, ________, TO(_QWERTY), ________, KC_0   , ________, ________,  KC_LEFT,  KC_RGHT
),
[_PUNCTUATION] = LAYOUT_planck_grid(
    ________,     KC_BSLS, LSFT(KC_LBRC), LSFT(KC_9), LSFT(KC_0), LSFT(KC_RBRC), ________, ________,     ________, ________, ________, ________,
    KC_TAB  ,  LSFT(KC_1),    LSFT(KC_2), LSFT(KC_4), LSFT(KC_7),    LSFT(KC_8), ________,   KC_EQL, LSFT(KC_EQL), ________, ________, ________,
    ________,  LSFT(KC_3),    LSFT(KC_5), LSFT(KC_6),   ________,      ________, ________, ________,      KC_LBRC,  KC_RBRC, ________, ________,
    ________,    ________,      ________,   ________,   ________,      ________, ________,   KC_GRV,     ________, ________, ________, ________
),
[_F] = LAYOUT_planck_grid(
    ________, ________, ________, ________, ________, ________, ________,   KC_F7, KC_F8  ,   KC_F9,  KC_F12, ________,
    ________, ________, ________, ________, ________, ________, ________,   KC_F4, KC_F5  ,   KC_F6,  KC_F11, ________,
    ________, ________, ________, ________, ________, ________, ________,   KC_F1, KC_F2  ,   KC_F3,  KC_F10, ________,
    ________, ________, ________, ________, ________, ________, ________, ________, ________, ________, ________, ________
),
[_IDE] = LAYOUT_planck_grid(
    MUTE_BRP, ________, STEP_OUT, EVALUATE, ________, ________, ________, ________, ________, ________, ________, ________,
    ________, ________,  STEP_IN, STEP_OVR, ________, ________, ________, ________, ________, ________, ________, ________,
    ________, ________, ________, ________, ________, ________, ________, ________, ________, ________, ________, ________,
    ________, ________, ________, ________, CONTINUE, ________, ________, ________, ________, ________, ________, ________
),
[_NAV] = LAYOUT_planck_grid( // When layer is active, LALT is held by macro
    ________, ________, ________, ________, ________, ________, ________,    ________, ________, ________, ________,CLOSE_WIN,
    ________, ________, ________, ________, ________, ________, ________, ALT_SFT_TAB,   KC_TAB, ________, ________, ________,
    ________, ________, ________, ________, ________, ________, ________,      KC_GRV, ________, ________, ________, ________,
    ________, ________, ________, ________, ________, ________, ________,    ________, ________, ________, ________, ________
)
};

LEADER_EXTERNS();

void matrix_scan_user(void) {

  LEADER_DICTIONARY() {
    leading = false;
    leader_end();

    SEQ_ONE_KEY(KC_F) {
      SEND_STRING("QMK is awesome.");
    }
    // IntelliJ
    SEQ_TWO_KEYS(KC_I, KC_R) {
      // Intellij > Open Recent
      SEND_STRING(SS_LCTL("0")"q");
    }
    SEQ_TWO_KEYS(KC_I, KC_C) {
      // Intellij > Close Tab
      SEND_STRING(SS_LCTL(SS_TAP(X_F4)));
    }
    SEQ_TWO_KEYS(KC_I, KC_A) {
      // Intellij > Actions
      SEND_STRING(SS_LCTL(SS_LSFT("a")));
    }
    SEQ_TWO_KEYS(KC_H, KC_L) {
      // History > Local
      SEND_STRING(SS_LALT("c")"h");
    }
    SEQ_THREE_KEYS(KC_H, KC_L, KC_S) {
      // History > Local > for Selection
      SEND_STRING(SS_LALT("c")"q");
    }
    SEQ_TWO_KEYS(KC_H, KC_G) {
      // History > Git
      SEND_STRING(SS_LALT("c")"g");
    }
    SEQ_THREE_KEYS(KC_H, KC_G, KC_S) {
      // History > Local > for Selection
      SEND_STRING(SS_LALT("c")"w");
    }
    // Breakpoints
    SEQ_TWO_KEYS(KC_B, KC_S) {
      // Breakpoint > Toggle
      SEND_STRING(SS_LCTL(SS_TAP(X_F8)));
    }
    SEQ_TWO_KEYS(KC_B, KC_R) {
      // Breakpoint > Remove all
      SEND_STRING(SS_LCTL("9")"r");
    }
  }
  
  if(IS_LAYER_OFF(_NAV)) {
     if(is_nav_layer_active) {
      SEND_STRING(SS_UP(X_LALT));
      is_nav_layer_active = false;
    }
  }
}

bool process_record_user(uint16_t keycode, keyrecord_t *record) {

  if(IS_LAYER_ON(_NAV)) {
    if(!is_nav_layer_active) {
      SEND_STRING(SS_DOWN(X_LALT));
      is_nav_layer_active = true;
    }
  }

  switch (keycode) {
    case URL_CATALOGORDER:
      if (record->event.pressed) {
	      SEND_STRING(SS_LCTL("l"));
      } else { // released
        SEND_STRING("https://localhost:8443/catalogOrder.htm");
      }
      break;
    case URL_CHECKOUTTUNNEL:
      if (record->event.pressed) {
	      SEND_STRING(SS_LCTL("l"));
      } else { // released
        SEND_STRING("https://localhost:8443/checkoutTunnel.htm");
      }
      break;
    case PASSWORD:
      if (record->event.pressed) {
        SEND_STRING("90708090");
      }
      break;
    case ALT_SFT_TAB:
      if (record->event.pressed) {
        SEND_STRING(SS_DOWN(X_LSFT));
        SEND_STRING(SS_TAP(X_TAB));
        SEND_STRING(SS_UP(X_LSFT));
      }
      break;
    case ESC_TAB:
      if (record->event.pressed) {
        if (get_mods() & MOD_MASK_ALT) {
          register_code(KC_TAB);
        } else {
          register_code(KC_ESC);
        }
      } else {
        unregister_code(KC_TAB);
        unregister_code(KC_ESC);
      }
      break;
  }
  return true;
}
